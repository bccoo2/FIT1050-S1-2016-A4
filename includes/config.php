<?php

ob_start();
session_start();

// db properties
define('DBHOST', 'sql205.freecluster.eu');
define('DBUSER', 'fceu_18152375');
define('DBPASS', 'Ac3CbEhTa');
define('DBNAME', 'fceu_18152375_abc');




// define site path
define('SITE_DOMAIN', 'http://blabla.rf.gd');

// define admin site path
define('SITE_DOMAIN_ADMIN', 'http://blabla.rf.gd/admin/');

// define site title for top of the browser
define('SITE_TITLE', 'FIT1050 - B. Cooper');

//define include checker
define('included', 1);

// make a connection to mysql here
$conn = new mysqli(DBHOST, DBUSER, DBPASS);
if ($conn->connect_error)
    die($conn->connect_error);
else
    mysqli_select_db($conn, DBNAME);

if (!defined('included'))
    die('You cannot access this file directly!');

/**
 * @param $user
 * @param $pass
 */
function login($user, $pass)
{
    global $conn;
    $user = strip_tags(mysqli_real_escape_string($conn, $user));
    $pass = strip_tags(mysqli_real_escape_string($conn, $pass));
    $pass = md5($pass);

    $result = mysqli_query($conn, "SELECT * FROM members WHERE username = '$user' AND password = '$pass'") or die($conn->mysqli_error());

    if (mysqli_num_rows($result) == 1) {
        $_SESSION['authorized'] = true;
        header('Location: ' . SITE_DOMAIN_ADMIN);
        exit();
    } else
        $_SESSION['error'] = 'Sorry, wrong username or password';
}

/**
 * @return bool
 */
function isSessionValid()
{
    if ($_SESSION['authorized'] == true)
        return true;
    else
        return false;

}

/**
 * @return bool
 */
function verifySession()
{
    if (isSessionValid())
        return true;
    else {
        header('Location: ' . SITE_DOMAIN_ADMIN . 'login.php');
        exit();
    }
}

/**
 * @unset authorized
 */
function logout()
{
    unset($_SESSION['authorized']);
    header('Location: ' . SITE_DOMAIN_ADMIN . 'login.php');
    exit();
}

/**
 * @echo message
 */
function messages()
{
    $message = '';
    if ($_SESSION['success'] != '') {
        $message = '<div class="messageOk">' . $_SESSION['success'] . '</div>';
        $_SESSION['success'] = '';
    }
    if ($_SESSION['error'] != '') {
        $message = '<div class="messageError">' . $_SESSION['error'] . '</div>';
        $_SESSION['error'] = '';
    }
    echo "$message";
}

/**
 * @param $error
 */
function errors($error)
{
    if (!empty($error)) {
        $i = 0;
        $showError = null;
        while ($i < count($error)) {
            $showError .= "<div class=\"msg-error\">" . $error[$i] . "</div>";
            $i++;
        }
        echo $showError;
    }
}