<?php
require('./includes/config.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo SITE_TITLE; ?></title>
    <link href="./style/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<!-- OPEN BODY WRAPPER -->
<div id="bodyWrapper">
    <!-- OPEN LOGO -->
    <div id="logoObject"><a href="<?php echo SITE_DOMAIN; ?>"><img src="style/images/logo.png"
                                                                   alt="<?php echo SITE_TITLE; ?>"
                                                                   title="<?php echo SITE_TITLE; ?>" border="0"/></a>
    </div>
    <!-- CLOSE LOGO -->

    <!-- OPEN NAVIGATION -->
    <div id="navigationWrapper">
        <ul class="navigationMenu">
            <li><a href="<?php echo SITE_DOMAIN; ?>">Home</a></li>
            <?php
            //get the rest of the pages
            $sql = mysqli_query($conn, "SELECT * FROM pages WHERE isRoot='1' ORDER BY pageID");
            while ($row = mysqli_fetch_object($sql)) {
                echo "<li><a href=\"" . SITE_DOMAIN . "?p=$row->pageID\">$row->pageTitle</a></li>";
            }
            ?>
        </ul>
    </div>
    <!-- CLOSE NAVIGATION -->

    <!-- OPEN CONTENT -->
    <div id="bodyContent">

        <?php
        if (!isset($_GET['p'])) {
            $q = mysqli_query($conn, "SELECT * FROM pages WHERE pageID='1'");
        } else {
            $id = mysqli_real_escape_string($conn, $_GET['p']);
            $q = mysqli_query($conn, "SELECT * FROM pages WHERE pageID='$id'");
        }

        //get page data from database and create an object
        $r = mysqli_fetch_object($q);

        //print the pages bodyContent
        echo "<h1>$r->pageTitle</h2>";
        echo $r->pageCont;
        ?>

    </div>
    <!-- CLOSE CONTENT -->

    <!-- OPEN FOOTER -->
    <div id="footerWrapper">
        <div class="copy">&copy; <?php echo SITE_TITLE . " " . date('Y'); ?> </div>
    </div>
    <!-- CLOSE FOOTER -->
</div>
<!-- CLOSE BODY WRAPPER -->
</body>
</html>