<?php
require('../includes/config.php');

if (isset($_POST['submit'])) {
    $title = mysqli_real_escape_string($conn, $_POST['pageTitle']);
    $content = mysqli_real_escape_string($conn, $_POST['pageCont']);

    mysqli_query($conn, "INSERT INTO pages (pageTitle,pageCont) VALUES ('$title','$content')") or die($conn->connect_error());
    $_SESSION['success'] = 'Page Added';
    header('Location: ' . SITE_DOMAIN_ADMIN);
    exit();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo SITE_TITLE; ?></title>
    <link href="../style/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<!-- OPEN BODY -->
<div id="bodyWrapper">
    <!-- OPEN LOGO -->
    <div id="logoObject"><a href="<?php echo SITE_DOMAIN; ?>"><img src="../style/images/logo.png" alt="<?php echo SITE_TITLE; ?>"
                                                                   title="<?php echo SITE_TITLE; ?>" border="0"/></a></div>
    <!-- CLOSE LOGO -->

    <!-- OPEN NAVIGATION -->
    <div id="navigationWrapper">
        <ul class="navigationMenu">
            <li><a href="<?php echo SITE_DOMAIN_ADMIN; ?>">Admin</a></li>
            <li><a href="<?php echo SITE_DOMAIN_ADMIN; ?>?logout">Logout</a></li>
            <li><a href="<?php echo SITE_DOMAIN; ?>" target="_blank">View Website</a></li>
        </ul>
    </div>
    <!-- CLOSE NAVIGATION -->

    <!-- OPEN CONTENT -->
    <div id="bodyContent">

        <h1>Add Page</h1>

        <form action="" method="post">
            <p>Title:<br/> <input name="pageTitle" type="text" value="" size="99"/></p>
            <p>content<br/><textarea name="pageCont" cols="133" rows="20"></textarea></p>
            <p><input type="submit" name="submit" value="Submit" class="button"/></p>
        </form>

    </div>
    <!-- CLOSE CONTENT -->
    
    <!-- OPEN FOOTER -->
    <div id="footerWrapper">
        <div class="copy">&copy; <?php echo SITE_TITLE . ' ' . date('Y'); ?> </div>
    </div>
    <!-- CLOSE FOOTER -->
</div>
<!-- CLOSE BODY -->
</body>
</html>