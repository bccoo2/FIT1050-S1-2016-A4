<?php
require('../includes/config.php');

//make sure user is logged in, function will redirect use if not logged in
verifySession();

//if logout has been clicked run the logout function which will destroy any active sessions and redirect to the login page
if (isset($_GET['logout'])) {
    logout();
}

//run if a page deletion has been requested
if (isset($_GET['delid'])) {

    $delid = mysqli_real_escape_string($conn, $_GET['delid']);
    mysqli_query($conn,"DELETE FROM pages WHERE pageID = '$delid'");
    $_SESSION['success'] = "Page Deleted";
    header('Location: ' . SITE_DOMAIN_ADMIN);
    exit();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo SITE_TITLE; ?></title>
    <link href="../style/style.css" rel="stylesheet" type="text/css"/>
    <script language="JavaScript" type="text/javascript">
        function delid(id, title) {
            if (confirm("Are you sure you want to delete '" + title + "'")) {
                window.location.href = '<?php echo SITE_DOMAIN_ADMIN;?>?delid=' + id;
            }
        }
    </script>
</head>
<body>
<!-- OPEN BODY -->
<div id="bodyWrapper">
    <!-- OPEN LOGO -->
    <div id="logoObject"><a href="<?php echo SITE_DOMAIN_ADMIN; ?>"><img src="../style/images/logo.png" alt="<?php echo SITE_TITLE; ?>"
                                                                         border="0"/></a></div>
    <!-- CLOSE LOGO -->

    <!-- OPEN NAVIGATION -->
    <div id="navigationWrapper">
        <ul class="navigationMenu">
            <li><a href="<?php echo SITE_DOMAIN_ADMIN; ?>">Admin</a></li>
            <li><a href="<?php echo SITE_DOMAIN; ?>" target="_blank">View Website</a></li>
            <li><a href="<?php echo SITE_DOMAIN_ADMIN; ?>?logout">Logout</a></li>
        </ul>
    </div>
    <!-- CLOSE NAVIGATION -->
    
    <!-- OPEN CONTENT -->
    <div id="bodyContent">

        <?php
        //show any messages if there are any.
        messages();
        ?>

        <h1>Manage Pages</h1>

        <table>
            <tr>
                <th><strong>Title</strong></th>
                <th><strong>Action</strong></th>
            </tr>

            <?php
            $sql = mysqli_query($conn, "SELECT * FROM pages ORDER BY pageID");
            while ($row = mysqli_fetch_object($sql)) {
                echo "<tr>";
                echo "<td>$row->pageTitle</td>";
                // Check if the page which is being removed is the "Home Page", if it is - Disable the edit function
                if ($row->pageID == 1) {
                    echo "<td><a href=\"" . SITE_DOMAIN_ADMIN . "editpage.php?id=$row->pageID\">Edit</a></td>";
                } else {
                    echo "<td><a href=\"" . SITE_DOMAIN_ADMIN . "editpage.php?id=$row->pageID\">Edit</a> | <a href=\"javascript:delid('$row->pageID','$row->pageTitle');\">Delete</a></td>";
                }

                echo "</tr>";
            }
            ?>
        </table>

        <p><a href="<?php echo SITE_DOMAIN_ADMIN; ?>addpage.php" class="button">Add Page</a></p>
    </div>
    <!-- CLOSE CONTENT -->
    
    <!-- OPEN FOOTER -->
    <div id="footerWrapper">
        <div class="copy">&copy; <?php echo SITE_TITLE . ' ' . date('Y'); ?> </div>
    </div>
    <!-- CLOSE FOOTER -->
</div>
<!-- CLOSE BODY -->
</body>
</html>